package archictecturerefcard01;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Controller
public class HomeController {

    @GetMapping(value = {"/index", "/"})
    public String home(Model model) {
        System.out.println("HomeController.home");

        Message message = new Message();

        try {
            System.out.println("HomeController.home " + InetAddress.getLocalHost().getHostAddress()
                  + " Date: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            message.setText("IP Address (Server): " + InetAddress.getLocalHost().getHostAddress()
                  + " Date: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        } catch(Exception e) {
            System.out.println("HomeController.home no ip address.");
            message.setText("IP Address (Server): ---");
        }

        model.addAttribute("message", message.getText());
        return "index";
    }

}
