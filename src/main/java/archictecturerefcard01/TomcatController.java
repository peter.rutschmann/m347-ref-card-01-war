package archictecturerefcard01;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author bbwpr
 * @version 15.05.2023
 */
@RestController
public class TomcatController {

   @GetMapping("/hello")
   public Collection<String> sayHello() {
      System.out.println("TomcatController.sayHello");
      return IntStream.range(0, 10)
            .mapToObj(i -> "Hello number " + i)
            .collect(Collectors.toList());
   }
}
