FROM tomcat:latest

# maven und git package installieren
RUN apt-get update

# Copy the build directory to tomcat document root
COPY target/*.war /usr/local/tomcat/webapps/

# Expose port 8080
EXPOSE 8080

# Start tomcat
CMD ["catalina.sh", "run"]